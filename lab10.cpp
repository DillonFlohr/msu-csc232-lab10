/**
 * CSC232 - Data Structures with C++, Fall 2016
 * @authors Jim Daehn, <Put Your Name Here>, <Put Your Name Here>
 *			Dillon Flohr (Driver)
 *			Benjamin Prescott (Navigator)
 * @brief Lab 10: Dynamic Programming - Minimum Liability Problem
 *        Due Date: 23:59 Saturday 12 November 2016
 */

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>

// Function Prototypes - You must implement these functions.

/**
 * Display the contents of a two-dimensional integer array.
 *
 * @param table the two dimensional array whose contents is displayed in tabular
 *       format
 * @param numRows the number of rows in the two dimensional array
 * @param numColumns the number of columns in the two dimensional array
 * @post The contents of table are displayed in tabular format to the standard
 *       output device. The contents of the table are unchanged.
 */
void display(int** table, const int& numRows, const int& numColumns);

/**
 * Calculate the minimum liability amassed in traveling from the
 * upper left-hand corner of a table to cell (m, n).
 *
 * @param matrix the two dimensional array whose contents liabilities
 * @param m the destination row in the matrix
 * @param n the destination column in the matrix
 * @return The minimum liability incurred in traversing the matrix from the upper
 *         left-hand corner to the destination cell (m, n)
 * @post The contents of the matrix are unchanged.
 */
int minLiability(int** matrix, const int& m, const int& n);

/**
 * Calculate the minimum value amongst three integers.
 *
 * @param a one integer in the comparison
 * @param b another integer in the comparison
 * @param c the third integer in the comparison
 * @return The minimum of a, b, c is returned.
 * @post Netier a nor b nor c is changed.
 */
int min(const int& a, const int& b, const int& c);

/**
 * Entry point of this application.
 *
 * @param argc the number of command-line arguments
 * @param argv the command-line arguments
 */
int main(int argc, char** argv) {
	// The data file containing matrix data
	std::string inputFile;
	// Check to see if the user has supplied a specific input file for consumption
	if (argc > 1) {
		inputFile = argv[1];
	} else {
		inputFile = "data.txt";
	}

	// A stream contining input data to process
	std::ifstream dataFile(inputFile, std::ios::in);

	// Check if stream is valid
	if (!dataFile) {
		std::cout << "Could not open " << inputFile << "..." << std::endl;
		// Invalid file, exit prematurely
		exit(EXIT_FAILURE);
	} else {
		// We have a valid file that is assumed to be properly formatted
		// As long as there is data to read...
		while (!dataFile.eof()) {
			// First items to read are the number of rows and columns for the current
			// matrix
			int rows;
			int cols;

			// Get the number of rows and columns for the current matrix
			dataFile >> rows >> cols;

			// The next set of items to be read are the target row and column
			int targetRow;
			int targetCol;
			dataFile >> targetRow >> targetCol;

			// create the array needed... a dynamic, two-dimensional array is an int**
			int **matrix;
			// Create the rows
			matrix = new int*[rows];
			// Create the columns
			for (int i { 0 }; i < rows; ++i) {
				matrix[i] = new int[cols];
			}

			// read data into array
			for (int row { 0 }; row < rows; ++row) {
				for (int col { 0 }; col < cols; ++col) {
					// read data from input file
					dataFile >> matrix[row][col];
				}
			}

			// Print liablity matrix
			display(matrix, rows, cols);

			// Compute the minimum liability
			int min = minLiability(matrix, targetRow, targetCol);
			std::cout << std::endl << "Minimum liability reaching ("
					<< targetRow << ", " << targetCol << ") is " << min
					<< std::endl << std::endl;

			// we're done processing the current matrix so let's get rid of it. If more
			// data exists, a new matrix with the appropriate dimensions will be created
			// in the next iteration of this loop.
			for (int i { 0 }; i < rows; ++i) {
				delete[] matrix[i];
				matrix[i] = nullptr;
			}
			delete[] matrix;
			matrix = nullptr;
		}
	}

	// No more data; program ends
	return EXIT_SUCCESS;
}

void display(int** matrix, const int& rows, const int& cols) {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j){
			std::cout << matrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

int minLiability(int** matrix, const int& targetRow, const int& targetCol) {
	// Crete a new two-dimensional array equal to the array matrix.
	int nMatrix[targetRow][targetCol];
	for (int i = 0; i <= targetRow; ++i) {
		for (int j = 0; j <= targetCol; ++j){
			nMatrix[i][j] =  matrix[i][j];
		}
	}
	//Fill top row with cost of getting to each position
	for (int n = 0; n <= targetCol; ++n) {
		if (n > 0) {
			nMatrix[0][n] = nMatrix[0][n-1] + nMatrix[0][n];
		}
	}
	//Fill first column with cost of getting to each position
	for (int m = 0; m <= targetRow; ++m) {
		if (m > 0) {
			nMatrix[m][0] = nMatrix[m-1][0] + nMatrix[m][0];
		}
	}
	//Fill the rest of the table with the vaules of the cost of getting
	//To that position.
	for (int c = 1; c <= targetRow; ++c) {
		for (int d = 1; d <= targetCol; ++d){
			nMatrix[c][d] = min(nMatrix[c-1][d], nMatrix[c][d-1], nMatrix[c-1][d-1]) + nMatrix[c][d];
		}
	}
	return nMatrix[targetRow][targetCol];
}

int min(const int& a, const int& b, const int& c) {
	int result{0};
	int finalResult{0};
	
	result = (a < b) ? a : b;
	
	finalResult = (result < c) ? result : c;
	
	return finalResult;
}
